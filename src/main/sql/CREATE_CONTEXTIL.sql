CREATE
DATABASE IF NOT EXISTS contextil;
USE
contextil;

CREATE TABLE `rol` (
                       `id_rol` int NOT NULL AUTO_INCREMENT,
                       `desc_rol` varchar(30) DEFAULT NULL,
                       PRIMARY KEY (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `rama` (
                        `id_rama` int NOT NULL AUTO_INCREMENT,
                        `desc_rama` varchar(255) NOT NULL,
                        PRIMARY KEY (`id_rama`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `usuario` (
                           `id_usuario` int NOT NULL AUTO_INCREMENT,
                           `apellido` varchar(255) NOT NULL,
                           `calificacion` decimal(10,0) DEFAULT NULL,
                           `descexp` mediumtext,
                           `email` varchar(255) NOT NULL,
                           `fechanac` date DEFAULT NULL,
                           `nombre` varchar(50) NOT NULL,
                           `password` varchar(1000) NOT NULL,
                           `radiocobertura` decimal(20,20) DEFAULT NULL,
                           `id_rama` int DEFAULT NULL,
                           `id_rol` int DEFAULT 2,
                           `telefono` int DEFAULT NULL,
                           PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `tarea` (
                         `id_tarea` int NOT NULL AUTO_INCREMENT,
                         `id_rama` int DEFAULT NULL,
                         `tipo_tarea` varchar(255) DEFAULT NULL,
                         PRIMARY KEY (`id_tarea`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `materiales` (
                              `id_materiales` int NOT NULL AUTO_INCREMENT,
                              `costo_materiales` double DEFAULT NULL,
                              `descrip_mat` varchar(255) DEFAULT NULL,
                              PRIMARY KEY (`id_materiales`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `pedido_cotizacion` (
                                     `id_ped_cot` int NOT NULL AUTO_INCREMENT,
                                     `desc_pedido` varchar(255) NOT NULL,
                                     `estado` int NOT NULL,
                                     `id_usuario_prest` int NOT NULL,
                                     `id_usuario_sol` int NOT NULL,
                                     PRIMARY KEY (`id_ped_cot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `cotizacion` (
                              `id_cotizacion` int NOT NULL AUTO_INCREMENT,
                              `alert_tx` date DEFAULT NULL,
                              `costo_cotizacion` double NOT NULL,
                              `estado_cot` int NOT NULL,
                              `fecha_fin` date DEFAULT NULL,
                              `fecha_ini` date DEFAULT NULL,
                              `id_materiales` int NOT NULL,
                              `id_ped_cot` int NOT NULL,
                              `id_tarea` int NOT NULL,
                              PRIMARY KEY (`id_cotizacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `trabajos_realizados` (
                                       `id_trab_real` int NOT NULL AUTO_INCREMENT,
                                       `calificacion` int DEFAULT NULL,
                                       `comentario` varchar(255) DEFAULT NULL,
                                       `id_transaccion` int DEFAULT NULL,
                                       PRIMARY KEY (`id_trab_real`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `transaccion` (
                               `id_transaccion` int NOT NULL AUTO_INCREMENT,
                               `alert_tx` date DEFAULT NULL,
                               `estado_tran` int DEFAULT NULL,
                               `fecha_inicio` date DEFAULT NULL,
                               `fecha_tx` date DEFAULT NULL,
                               `id_cotizacion` int DEFAULT NULL,
                               PRIMARY KEY (`id_transaccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

);