INSERT INTO rama (id_rama, desc_rama)
VALUES (1, 'Modista'),
       (2, 'Planchadores'),
       (3, 'Cortador'),
       (4, 'Tallerista'),
       (5, 'Encimador'),
       (6, 'Moldista'),
       (7, 'Alta costura'),
       (8, 'Taller'),
       (9, 'Tizador'),
       (10, 'Muestrista'),
       (11, 'Otros');


insert into contextil.rol(id_rol, desc_rol)
values (1,'admin'),
       (2, 'usuario solicitante'),
       (3, 'usuario prestador');

insert into contextil.usuario (id_usuario, nombre, apellido, email, password, rol)
values (1, 'Taffy', 'Fromont', 'tfromont0@sourceforge.net', 'xWEMRNKZZ8@l4pwj', 2),
       (2, 'Lorine', 'Lumm', 'llumm1@tmall.com', 'gICOWPSRI8=', 2),
       (3, 'Dorian', 'Jeynes', 'djeynes2@cyberchimps.com', 'mXRXPMNQZ6,/', 2),
       (4, 'Antonie', 'Faveryear', 'afaveryear3@google.fr', 'uUCRHGJPH6"', 2),
       (5, 'Marty', 'Shillum', 'mshillum4@vkontakte.ru', 'pIYURFSXZ2%', 2),
       (6, 'Angie', 'Dunican', 'adunican5@comcast.net', 'wFKQPCQFE6,_Ts', 2),
       (7, 'Nicolina', 'Bellee', 'nbellee6@ftc.gov', 'xYQKGXPSL2>', 2),
       (8, 'Lorraine', 'Greig', 'lgreig7@hostgator.com', 'aIYBJFAVB7#', 2),
       (9, 'Lib', 'Eldrid_usuario', 'leldrid_usuario8@mtv.com', 'wIKJLKZDQ4"', 2),
       (10, 'Berky', 'Bestar', 'bbestar9@google.ca', 'qFWBXTQYT1"zu', 2);
