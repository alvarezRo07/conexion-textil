package com.conexiontextil.model;

import jakarta.persistence.*;
import org.springframework.data.annotation.Id;

@Entity
@Table(name = "rama")
public class RamaLaboral {

    @jakarta.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idRama;

    @Column(name = "descRama", nullable = false, length = 255)
    private String descRama;

    public RamaLaboral(String descRama) {
        this.descRama = descRama;
    }

    public RamaLaboral() {

    }

    public String getDescRama() {
        return descRama;
    }

    public void setDescRama(String descRama) {
        this.descRama = descRama;
    }

    public Integer getIdRama() {
        return idRama;
    }

    public void setIdRama(Integer idRama) {
        this.idRama = idRama;
    }
}
