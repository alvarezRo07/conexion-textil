package com.conexiontextil.model;

import jakarta.persistence.*;
import org.springframework.data.annotation.Id;

@Entity
@Table(name = "pedido_cotizacion")
public class PedidoCotizacion {

    @jakarta.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPedCot;

    //TODO machear tipo de dato id
    @Column(name = "id_usuario_prest", nullable = false)
    private Integer usuarioPrest;

    @Column(name = "id_usuario_sol", nullable = false)
    private Integer usuarioSol;

    @Column(name = "estado", nullable = false)
    private Integer estado;

    @Column(name = "desc_pedido", nullable = false, length = 255)
    private String descPedido;

    // Getters and Setters
    public Integer getIdPedCot() {
        return idPedCot;
    }

    public void setIdPedCot(Integer idPedCot) {
        this.idPedCot = idPedCot;
    }

    public Integer getUsuarioPrest() {
        return usuarioPrest;
    }

    public void setUsuarioPrest(Integer usuarioPrest) {
        this.usuarioPrest = usuarioPrest;
    }

    public Integer getUsuarioSol() {
        return usuarioSol;
    }

    public void setUsuarioSol(Integer usuarioSol) {
        this.usuarioSol = usuarioSol;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getDescPedido() {
        return descPedido;
    }

    public void setDescPedido(String descPedido) {
        this.descPedido = descPedido;
    }
}
