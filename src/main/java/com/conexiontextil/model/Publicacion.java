package com.conexiontextil.model;

import jakarta.persistence.*;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Entity
@Table(name = "publicacion")
public class Publicacion {
    @jakarta.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPublicacion;

    @JoinColumn(name = "id_usuario", nullable = false )
    private Integer idUsuario;

    @Column(name="horario", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date horario;

    @JoinColumn(name="id_barrio", nullable = false)
    private Integer idBarrio;

    public Publicacion() {
    }

    public Publicacion(Integer idPublicacion, Integer idUsuario, Date horario, Integer idBarrio) {
        this.idPublicacion = idPublicacion;
        this.idUsuario = idUsuario;
        this.horario = horario;
        this.idBarrio = idBarrio;
    }

    public Integer getIdPublicacion() {return idPublicacion;}
    public void setIdPublicacion(Integer idPublicacion) {this.idPublicacion = idPublicacion;}

    public Integer getIdUsuario() {return idUsuario;}
    public void setIdUsuario(Integer idUsuario) {this.idUsuario = idUsuario;}

    public Date getHorario() {return horario;}
    public void setHorario(Date horario) {this.horario = horario;}

    public Integer getIdBarrio() {return idBarrio;}
    public void setIdBarrio(Integer idBarrio) {this.idBarrio = idBarrio;}

}
