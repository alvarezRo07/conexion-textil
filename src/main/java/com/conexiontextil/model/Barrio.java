package com.conexiontextil.model;

import jakarta.persistence.*;
import org.springframework.data.annotation.Id;

@Entity
@Table(name= "barrio")
public class Barrio {

    @Id
    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idBarrio;

    @Column(name = "latitud")
    private double latitud;

    @Column(name= "longitud")
    private double longitud;

    @Column(name = "barrio_desc")
    private String barrioDesc;

    @Column(name= "place_id")
    private Integer placeId;

    public Barrio() {
    }

    public Barrio(double latitud, double longitud, String barrioDesc, Integer placeId) {
        this.latitud = latitud;
        this.longitud = longitud;
        this.barrioDesc = barrioDesc;
        this.placeId = placeId;
    }

    public Integer getIdBarrio() {
        return idBarrio;
    }
    public void setIdBarrio(Integer idBarrio) {
        this.idBarrio = idBarrio;
    }
    public double getLatitud() {
        return latitud;
    }
    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }
    public double getLongitud() {
        return longitud;
    }
    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }
    public String getBarrioDesc() {
        return barrioDesc;
    }
    public void setBarrioDesc(String barrioDesc) {
        this.barrioDesc = barrioDesc;
    }
    public Integer getPlaceId() {
        return placeId;
    }
    public void setPlaceId(Integer placeId) {
        this.placeId = placeId;
    }
}
