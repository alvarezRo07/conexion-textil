package com.conexiontextil.model;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Table;
import org.springframework.data.annotation.Id;


@Entity
@Table(name = "materiales")
public class Materiales {

    @jakarta.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idMateriales;

    private String descripMat;
    private Double costoMateriales;

    // Getters y Setters genéricos
    public Integer getIdMateriales() {
        return idMateriales;
    }

    public void setIdMateriales(Integer idMateriales) {
        this.idMateriales = idMateriales;
    }

    public String getDescripMat() {
        return descripMat;
    }

    public void setDescripMat(String descripMat) {
        this.descripMat = descripMat;
    }

    public Double getCostoMateriales() {
        return costoMateriales;
    }

    public void setCostoMateriales(Double costoMateriales) {
        this.costoMateriales = costoMateriales;
    }
}
