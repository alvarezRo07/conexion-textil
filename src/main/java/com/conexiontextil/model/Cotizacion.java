package com.conexiontextil.model;

import jakarta.persistence.*;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Entity
@Table(name = "cotizacion")
public class Cotizacion {

    @Id
    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCotizacion;

    @Column(name = "id_ped_cot", nullable = false)
    private Integer idPedCot;

    @Column(name = "id_materiales", nullable = false)
    private Integer idMateriales;

    @Column(name = "id_tarea", nullable = false)
    private Integer idTarea;

    @Column(name = "costo_cotizacion", nullable = false)
    private Double costoCotizacion;

    @Column (name = "fecha_ini")
    @Temporal(TemporalType.DATE)
    private Date fechaIni;

    @Column (name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;

    @Column (name = "alert_tx")
    @Temporal(TemporalType.DATE)
    private Date alertTx;

    @Column(name = "estado_cot", nullable = false)
    private Integer estadoCot;


    public Cotizacion() {
    }

    public Cotizacion(Integer idPedCot, Integer idMateriales, Integer idTarea, Double costoCotizacion, Date fechaIni, Date fechaFin, Date alertTx, Integer estadoCot) {
        this.idPedCot = idPedCot;
        this.idMateriales = idMateriales;
        this.idTarea = idTarea;
        this.costoCotizacion = costoCotizacion;
        this.fechaIni = fechaIni;
        this.fechaFin = fechaFin;
        this.alertTx = alertTx;
        this.estadoCot = estadoCot;
    }

    public Integer getIdCotizacion() {
        return idCotizacion;
    }

    public void setIdCotizacion(Integer idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    public Integer getIdPedCot() {
        return idPedCot;
    }

    public void setIdPedCot(Integer idPedCot) {
        this.idPedCot = idPedCot;
    }

    public Integer getIdMateriales() {

        return idMateriales;
    }

    public void setIdMateriales(Integer idMateriales) {

        this.idMateriales = idMateriales;
    }

    public Integer getIdTarea() {
        return idTarea;
    }

    public void setIdTarea(Integer idTarea) {
        this.idTarea = idTarea;
    }

    public Double getCostoCotizacion() {
        return costoCotizacion;
    }

    public void setCostoCotizacion(Double costoCotizacion) {
        this.costoCotizacion = costoCotizacion;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getAlertTx() {
        return alertTx;
    }

    public void setAlertTx(Date alertTx) {
        this.alertTx = alertTx;
    }

    public Integer getEstadoCot() {
        return estadoCot;
    }

    public void setEstadoCot(Integer estadoCot) {
        this.estadoCot = estadoCot;
    }
}
