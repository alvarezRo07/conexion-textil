package com.conexiontextil.model;


import jakarta.persistence.*;
import org.springframework.data.annotation.Id;


import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "usuario")
public class Usuario {

    @jakarta.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idUsuario;

    //@ManyToOne
    @JoinColumn(name = "id_rol", nullable = false)
    private Integer rol;

    //@ManyToOne
    @JoinColumn(name = "idRama", nullable = false)
    private Integer ramaLaboral;

    @Column(name = "nombre", nullable = false,  length = 50)
    private String nombre;

    @Column(name = "apellido", nullable = false, length = 255)
    private String apellido;

    @Column(name = "email", nullable = false, length = 255)
    private String email;

    @Column(name = "password", nullable = false, length = 1000)
    private String password;

    @Column(name = "telefono")
    private Integer telefono;

    @Column(name = "fechanac")
    @Temporal(TemporalType.DATE)
    private Date fechaNac;

    @Column(name = "radiocobertura", precision = 20, scale = 20)
    private BigDecimal radioCobertura;

    @Column(name = "descexp", columnDefinition = "MEDIUMTEXT")
    private String descripcionExperiencia;

    @Column(name = "calificacion", precision = 10)
    private BigDecimal calificacion;

    @Column(name="barrio")
    private Integer barrio;

    @Column(name="esmovil")
    private Integer esmovil;


    @Transient
    private String imageUrl;

    public Usuario() {
    }

    public Usuario(Integer rol, String nombre, String apellido, String email, String password, Date fechaNac, Integer ramaLaboral, Integer barrio, Integer esmovil) {
        this.rol = rol;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.password = password;
        this.fechaNac = fechaNac;
        this.ramaLaboral = ramaLaboral;
        this.barrio = barrio;
        this.esmovil = esmovil;
    }

    public Usuario(Integer rol, String nombre, String apellido, String email, String password, Date fechaNac) {
        this.rol = rol;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.password = password;
        this.fechaNac = fechaNac;
    }

    public Usuario(String nombre, String apellido, String email, String password, Date fechaNac) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.password = password;
        this.fechaNac = fechaNac;
    }




    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getRol() {
        return rol;
    }

    public void setRol(Integer rol) {
        this.rol = rol;
    }

    public Integer getRamaLaboral() {
        return ramaLaboral;
    }

    public void setRamaLaboral(Integer ramaLaboral) {
        this.ramaLaboral = ramaLaboral;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    public BigDecimal getRadioCobertura() {
        return radioCobertura;
    }

    public void setRadioCobertura(BigDecimal radioCobertura) {
        this.radioCobertura = radioCobertura;
    }

    public String getDescripcionExperiencia() {
        return descripcionExperiencia;
    }

    public void setDescripcionExperiencia(String descExp) {
        this.descripcionExperiencia = descExp;
    }

    public BigDecimal getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(BigDecimal calificacion) {
        this.calificacion = calificacion;
    }

    public Integer getBarrio() { return barrio;}

    public void setBarrio(Integer barrio) { this.barrio = barrio; }

    public Integer isEsmovil() { return esmovil; }

    public void setEsmovil(Integer esmovil) { this.esmovil = esmovil; }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
