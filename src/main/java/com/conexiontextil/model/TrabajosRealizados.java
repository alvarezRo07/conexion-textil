package com.conexiontextil.model;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Table;
import org.springframework.data.annotation.Id;


@Entity
@Table(name = "trabajos_realizados")
public class TrabajosRealizados {

    @Id
    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idTrabReal;

    private Integer idTransaccion;
    private Integer calificacion;
    private String comentario;

    // Getters y Setters genéricos
    public Integer getIdTrabReal() {
        return idTrabReal;
    }

    public void setIdTrabReal(Integer idTrabReal) {
        this.idTrabReal = idTrabReal;
    }

    public Integer getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Integer getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Integer calificacion) {
        this.calificacion = calificacion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
}
