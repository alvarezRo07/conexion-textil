package com.conexiontextil.model;

import jakarta.persistence.*;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Entity
@Table(name = "transaccion")
public class Transaccion {

    @jakarta.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idTransaccion;

    private Integer idCotizacion;
    private Integer estadoTran;

    @Temporal(TemporalType.DATE)
    private Date fechaTx;

    @Temporal(TemporalType.DATE)
    private Date alertTx;

    @Temporal(TemporalType.DATE)
    private Date fechaInicio;

    public Transaccion() {
    }

    public Transaccion(Integer idCotizacion, Integer estadoTran, Date fechaTx, Date alertTx, Date fechaInicio) {
        this.idCotizacion = idCotizacion;
        this.estadoTran = estadoTran;
        this.fechaTx = fechaTx;
        this.alertTx = alertTx;
        this.fechaInicio = fechaInicio;
    }

    public Integer getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Integer getIdCotizacion() {
        return idCotizacion;
    }

    public void setIdCotizacion(Integer idCotizacion) {
        this.idCotizacion = idCotizacion;
    }

    public Integer getEstadoTran() {
        return estadoTran;
    }

    public void setEstadoTran(Integer estadoTran) {
        this.estadoTran = estadoTran;
    }

    public Date getFechaTx() {
        return fechaTx;
    }

    public void setFechaTx(Date fechaTx) {
        this.fechaTx = fechaTx;
    }

    public Date getAlertTx() {
        return alertTx;
    }

    public void setAlertTx(Date alertTx) {
        this.alertTx = alertTx;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
}
