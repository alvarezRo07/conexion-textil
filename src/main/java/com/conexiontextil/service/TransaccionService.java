package com.conexiontextil.service;

import com.conexiontextil.model.Transaccion;
import com.conexiontextil.repository.TransaccionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TransaccionService {

    @Autowired
    private TransaccionRepository transaccionRepository;

    public List<Transaccion> getAllTransacciones() {
        return transaccionRepository.findAll();
    }

    public Optional<Transaccion> getTransaccionById(Integer id) {
        return transaccionRepository.findById(id);
    }

    public Transaccion createTransaccion(Transaccion transaccion) {
        return transaccionRepository.save(transaccion);
    }

    public Optional<Transaccion> updateTransaccion(Integer id, Transaccion transaccionDetails) {
        return transaccionRepository.findById(id).map(transaccion -> {
            transaccion.setFechaInicio(transaccionDetails.getFechaInicio());
            return transaccionRepository.save(transaccion);
        });
    }

    public boolean deleteTransaccion(Integer id) {
        return transaccionRepository.findById(id).map(transaccion -> {
            transaccionRepository.delete(transaccion);
            return true;
        }).orElse(false);
    }
}
