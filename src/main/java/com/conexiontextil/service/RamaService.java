package com.conexiontextil.service;

import com.conexiontextil.model.RamaLaboral;
import com.conexiontextil.repository.RamaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RamaService {

    @Autowired
    private RamaRepository ramaRepository;

    public List<RamaLaboral> getAllRamas() {
        return ramaRepository.findAll();
    }

    public Optional<RamaLaboral> getRamaById(Integer id) {
        return ramaRepository.findById(id);
    }

    public RamaLaboral createRama(RamaLaboral ramaLaboral) {
        return ramaRepository.save(ramaLaboral);
    }

    public Optional<RamaLaboral> updateRama(Integer id, RamaLaboral ramaLaboralDetails) {
        return ramaRepository.findById(id).map(ramaLaboral -> {
            ramaLaboral.setDescRama(ramaLaboralDetails.getDescRama());
            return ramaRepository.save(ramaLaboral);
        });
    }

    public boolean deleteRama(Integer id) {
        return ramaRepository.findById(id).map(ramaLaboral -> {
            ramaRepository.delete(ramaLaboral);
            return true;
        }).orElse(false);
    }
}
