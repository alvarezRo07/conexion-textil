package com.conexiontextil.service;

import com.conexiontextil.model.Usuario;
import com.conexiontextil.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario readUsuario(String nombre) {
        return usuarioRepository.findByNombre(nombre);
    }

    public Optional<Usuario> readUsuario(Integer usuarioID) {
        return usuarioRepository.findById(usuarioID);
    }

    public Optional<Usuario> readUsuarioByMail(String email) {
        return usuarioRepository.findByEmail(email);
    }

    public void createUsuario(Usuario usuario) {
        usuarioRepository.save(usuario);
    }

    public List<Usuario> listUsuarios() {
        return usuarioRepository.findAll();
    }

    public void updateUsuario(Integer usuarioID, Usuario newUsuario) {
        Usuario usuario = usuarioRepository.findById(usuarioID).get();
        usuario.setNombre(newUsuario.getNombre());
        usuario.setDescripcionExperiencia(usuario.getDescripcionExperiencia());
        usuario.setImageUrl(usuario.getImageUrl());
        usuarioRepository.save(usuario);
    }
    public void deleteUsuario(Integer idUsuario) {
        usuarioRepository.deleteById(idUsuario);
    }
    public Optional<Usuario> authenticateUsuario(String email, String password) {
        Optional<Usuario> usuario = usuarioRepository.findByEmail(email);
        if (usuario.isPresent() && usuario.get().getPassword().equals(password)) {
            return usuario;
        } else {
            return Optional.empty();
        }
    }
}