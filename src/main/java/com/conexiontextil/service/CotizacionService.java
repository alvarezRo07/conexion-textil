package com.conexiontextil.service;

import com.conexiontextil.model.Cotizacion;
import com.conexiontextil.repository.CotizacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CotizacionService {

    @Autowired
    private CotizacionRepository cotizacionRepository;

    public List<Cotizacion> getAllCotizaciones() {
        return cotizacionRepository.findAll();
    }

    public Optional<Cotizacion> getCotizacionById(Integer id) {
        return cotizacionRepository.findById(id);
    }

    public Cotizacion createCotizacion(Cotizacion cotizacion) {
        return cotizacionRepository.save(cotizacion);
    }

    public Optional<Cotizacion> updateCotizacion(Integer id, Cotizacion cotizacionDetails) {
        return cotizacionRepository.findById(id).map(cotizacion -> {

            return cotizacionRepository.save(cotizacion);
        });
    }

    public boolean deleteCotizacion(Integer id) {
        return cotizacionRepository.findById(id).map(cotizacion -> {
            cotizacionRepository.delete(cotizacion);
            return true;
        }).orElse(false);
    }
}
