package com.conexiontextil.common;
public enum Estado {
    EN_CURSO,
    FINALIZADO,
    CERRADO
}
