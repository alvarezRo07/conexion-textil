package com.conexiontextil.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateService {

    public static Date parseDate(String fecha){
        Date fechaFormateada = new Date();
         try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            fechaFormateada= dateFormat.parse(fecha);

        }catch (
            ParseException e){
            e.printStackTrace();
        }
        return fechaFormateada;
    }
}
