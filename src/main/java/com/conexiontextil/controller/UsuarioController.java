package com.conexiontextil.controller;

import com.conexiontextil.common.ApiResponse;
import com.conexiontextil.common.DateService;
import com.conexiontextil.model.Usuario;
import com.conexiontextil.repository.RolRepository;
import com.conexiontextil.service.UsuarioService;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("/usuario")
@Tag(name = "Usuarios", description = "Gestión de Usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;
    private RolRepository rolRepository;

    List<Integer> roles = Arrays.asList(1, 2, 3);

    @PostMapping("/create")
    @Operation(summary = "Crear un nuevo usuario", description = "Crea un nuevo usuario")
    public ResponseEntity<ApiResponse> createUsuario(@Valid @RequestBody Usuario usuario) {
         if (Objects.nonNull(usuarioService.readUsuarioByMail(usuario.getEmail()))) {
            return new ResponseEntity<>(new ApiResponse(false, "El mail ingresado ya existe"), HttpStatus.CONFLICT);
        }
        usuarioService.createUsuario(usuario);
        return new ResponseEntity<>(new ApiResponse(true, "Usuario creado exitosamente"), HttpStatus.CREATED);
    }

    @GetMapping("/")
    @Operation(summary = "Obtener todos los usuarios", description = "Obtiene una lista de todos los usuarios")
    public ResponseEntity<List<Usuario>> getUsuarios() {
        List<Usuario> body = usuarioService.listUsuarios();
        return new ResponseEntity<>(body, HttpStatus.OK);
    }

    @PostMapping("/create{usuarioID}")
    @Operation(summary = "Actualizar un usuario", description = "Actualiza un usuario existente por su ID")
    public ResponseEntity<ApiResponse> updateUsuario(@PathVariable("usuarioID") Integer usuarioID, @Valid @RequestBody Usuario usuario) {
        if (Objects.nonNull(usuarioService.readUsuario(usuarioID))) {
            usuarioService.updateUsuario(usuarioID, usuario);
            return new ResponseEntity<>(new ApiResponse(true, "Usuario actualizado exitosamente"), HttpStatus.OK);
        }
        return new ResponseEntity<>(new ApiResponse(false, "El usuario no existe"), HttpStatus.NOT_FOUND);
    }

    @PostMapping("/crearConParametros")
    @Operation(summary = "Crear un nuevo usuario con parámetros", description = "Crea un nuevo usuario usando parámetros específicos")
    public ResponseEntity<ApiResponse> createUsuarioConParametros(
            @RequestParam Integer rolUsuario,
            @RequestParam String nombre,
            @RequestParam String apellido,
            @RequestParam String email,
            @RequestParam String password,
            @RequestParam @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy") String fechaNac) {

            Usuario usuario = new Usuario(rolUsuario, nombre, apellido, email, password, DateService.parseDate(fechaNac));

         if (usuarioService.readUsuarioByMail(email).isPresent()) {
            return new ResponseEntity<>(new ApiResponse(false, "El mail ingresado ya existe"), HttpStatus.CONFLICT);
        }

        usuarioService.createUsuario(usuario);
        return new ResponseEntity<>(new ApiResponse(true, "Usuario creado exitosamente"), HttpStatus.CREATED);
    }

    @PostMapping("/crearUsuarioPrestador")
    @Operation(summary = "Crear usuario Prestador", description = "Crea un nuevo usuario prestador")
    public ResponseEntity<ApiResponse> crearUsuarioPrestador(
            @RequestParam Integer rolUsuario,
            @RequestParam String nombre,
            @RequestParam String apellido,
            @RequestParam String email,
            @RequestParam String password,
            @RequestParam @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy") String fechaNac,
            @RequestParam Integer ramaLaboral,
            @RequestParam Integer barrio,
            @RequestParam boolean esMovil)
    {

        Usuario usuario = new Usuario(rolUsuario, nombre, apellido, email, password, DateService.parseDate(fechaNac), ramaLaboral, barrio, (esMovil? 1 : 0) );

        if (usuarioService.readUsuarioByMail(email).isPresent()) {
            return new ResponseEntity<>(new ApiResponse(false, "El mail ingresado ya existe"), HttpStatus.CONFLICT);
        }

        usuarioService.createUsuario(usuario);
        return new ResponseEntity<>(new ApiResponse(true, "Usuario creado exitosamente"), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Borrar un usuario por ID", description = "Borra un usuario específico por su ID")
    public ResponseEntity<ApiResponse> deleteUsuario(@PathVariable("id") Integer idUsuario) {
        if (Objects.nonNull(usuarioService.readUsuario(idUsuario))) {
            usuarioService.deleteUsuario(idUsuario);
            return new ResponseEntity<>(new ApiResponse(true, "Usuario borrado exitosamente"), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new ApiResponse(false, "Usuario no encontrado"), HttpStatus.NOT_FOUND);
        }
    }
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody Usuario loginRequest) {
        Optional<Usuario> usuario = usuarioService.authenticateUsuario(loginRequest.getEmail(), loginRequest.getPassword());
        if (usuario.isPresent()) {
            return ResponseEntity.ok(usuario.get());
        } else {
            return ResponseEntity.status(401).body("Invalid email or password");
        }
    }

}

