package com.conexiontextil.controller;

import com.conexiontextil.model.Cotizacion;
import com.conexiontextil.repository.CotizacionRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/cotizaciones")
@Tag(name = "Cotizaciones", description = "Gestión de Cotizaciones")
public class CotizacionController {

    @Autowired
    private CotizacionRepository cotizacionRepository;

    @GetMapping
    @Operation(summary = "Obtener todas las cotizaciones", description = "Obtiene una lista de todas las cotizaciones")
    public List<Cotizacion> getAllCotizaciones() {
        return cotizacionRepository.findAll();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Obtener cotización por ID", description = "Obtiene una cotización específica por su ID")
    public ResponseEntity<Cotizacion> getCotizacionById(@PathVariable Integer id) {
        Optional<Cotizacion> cotizacion = cotizacionRepository.findById(id);
        return cotizacion.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    @Operation(summary = "Crear una nueva cotización", description = "Crea una nueva cotización")
    public Cotizacion createCotizacion(@RequestBody Cotizacion cotizacion) {
        return cotizacionRepository.save(cotizacion);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Actualizar una cotización", description = "Actualiza una cotización existente por su ID")
    public ResponseEntity<Cotizacion> updateCotizacion(@PathVariable Integer id, @RequestBody Cotizacion cotizacionDetails) {
        Optional<Cotizacion> cotizacion = cotizacionRepository.findById(id);
        if (cotizacion.isPresent()) {
            Cotizacion updatedCotizacion = cotizacion.get();
            cotizacionRepository.save(updatedCotizacion);
            return ResponseEntity.ok(updatedCotizacion);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Eliminar una cotización", description = "Elimina una cotización existente por su ID")
    public ResponseEntity<Void> deleteCotizacion(@PathVariable Integer id) {
        Optional<Cotizacion> cotizacion = cotizacionRepository.findById(id);
        if (cotizacion.isPresent()) {
            cotizacionRepository.delete(cotizacion.get());
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
