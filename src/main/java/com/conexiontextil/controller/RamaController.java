package com.conexiontextil.controller;

import com.conexiontextil.model.RamaLaboral;
import com.conexiontextil.service.RamaService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/ramas")
@Tag(name = "Ramas", description = "Gestión de Ramas")
public class RamaController {

    @Autowired
    private RamaService ramaService;

    @GetMapping
    @Operation(summary = "Obtener todas las ramas", description = "Obtiene una lista de todas las ramas")
    public List<RamaLaboral> getAllRamas() {
        return ramaService.getAllRamas();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Obtener rama por ID", description = "Obtiene una rama específica por su ID")
    public ResponseEntity<RamaLaboral> getRamaById(@PathVariable Integer id) {
        Optional<RamaLaboral> rama = ramaService.getRamaById(id);
        return rama.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    @Operation(summary = "Crear una nueva rama", description = "Crea una nueva rama")
    public RamaLaboral createRama(@RequestBody RamaLaboral ramaLaboral) {
        return ramaService.createRama(ramaLaboral);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Actualizar una rama", description = "Actualiza una rama existente por su ID")
    public ResponseEntity<RamaLaboral> updateRama(@PathVariable Integer id, @RequestBody RamaLaboral ramaLaboralDetails) {
        Optional<RamaLaboral> updatedRama = ramaService.updateRama(id, ramaLaboralDetails);
        return updatedRama.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Eliminar una rama", description = "Elimina una rama existente por su ID")
    public ResponseEntity<Void> deleteRama(@PathVariable Integer id) {
        boolean isDeleted = ramaService.deleteRama(id);
        return isDeleted ? ResponseEntity.noContent().build() : ResponseEntity.notFound().build();
    }
}
