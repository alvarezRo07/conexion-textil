package com.conexiontextil.repository;

import com.conexiontextil.model.TrabajosRealizados;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrabajosRealizadosRepository extends JpaRepository<TrabajosRealizados, Integer> {
}
