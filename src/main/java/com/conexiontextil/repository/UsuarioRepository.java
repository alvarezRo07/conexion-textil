package com.conexiontextil.repository;


import com.conexiontextil.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository  extends JpaRepository <Usuario, Integer> {

    Usuario findByNombre(String nombre);

    Optional<Usuario> findById(Integer usuarioID);

    Optional<Usuario> findByEmail(String email);

    List<Usuario> findAll();
}
