package com.conexiontextil.repository;

import com.conexiontextil.model.Materiales;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MaterialesRepository extends JpaRepository<Materiales, Integer> {
}
