package com.conexiontextil.repository;

import com.conexiontextil.model.PedidoCotizacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PedidoCotizacionRepository extends JpaRepository<PedidoCotizacion, Integer> {
}
