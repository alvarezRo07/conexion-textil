package com.conexiontextil.repository;

import com.conexiontextil.model.RamaLaboral;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RamaRepository extends JpaRepository<RamaLaboral, Integer> {
}
