package com.conexiontextil.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.conexiontextil.model.Barrio;

@Repository
public interface BarrioRepository extends JpaRepository<Barrio, Integer> {
}
